const wordBox = document.getElementById("wordinput");
const gamemodeBox = document.getElementById("gminput");
const start = document.getElementById("submitword");
const oneLetterScoreGiven = 20;
let attemptCounter = 0;
let scoreBoost = 0;
let scoreCounter = 0;
let scoreBoostChangeable = 1;
let firstWord = '';
let gamemode = '';
let winCheck = 0;


start.addEventListener('click', e => {
    e.preventDefault();
    if (wordBox.value === '' || (gamemodeBox.value.toLowerCase() !== 'hard' && gamemodeBox.value.toLowerCase() !== 'medium' && gamemodeBox.value.toLowerCase() !== 'easy')) {
        alert("Game mode or word input incorrectly, available game modes: hard, medium, easy")
    } else {
        firstWord = wordBox.value
        gamemode = gamemodeBox.value
        if (gamemode.toLowerCase() === "hard") {
            attemptCounter = 3;
            scoreBoost = 1.5;
        } else if (gamemode.toLowerCase() === "medium") {
            attemptCounter = 7;
            scoreBoost = 1.2;
        } else if (gamemode.toLowerCase() === "easy") {
            attemptCounter = 10;
            scoreBoost = 1;
        }
        const afPage = document.getElementById("mainpage")
        const prePage = document.getElementById("prepage")
        afPage.className = "af-page"
        prePage.className = "not-shown"
        start.removeEventListener('click', e);
        const attChecker = attemptCounter;

        function cubeGeneration(word) {
            const wordContainer = document.getElementById("word-container");
            for (let i = 0; i < word.length; i++) {
                const letterTemporal = document.createElement("p");
                letterTemporal.classList.add("cube")
                if (word[i] === ' ') {
                    letterTemporal.classList.add("cube-space")
                    word[i] = "+"
                    winCheck += 1;
                }
                wordContainer.append(letterTemporal);
            }
        }

        cubeGeneration(firstWord);

        atts = document.getElementById("attempts");
        let word = firstWord.split('');
        let k = 0;
        let maxBoost = 0;
        const allPs = document.querySelectorAll(".cube");
        const boostStatus = document.getElementById("booster")
        atts.textContent = `Attempts remaining ${attemptCounter}`;
        boostStatus.textContent = `Streak: x${scoreBoostChangeable-1}`

        function mainScript(word, letter) {
            k = 0
            const letterUsedList = document.getElementById("letterCounter");
            if (letterUsedList.textContent.includes(letter) === false) {
                letterUsedList.textContent += letter + ' ';
            }
            word.forEach(function(value, letterIndex) {
                if (value.toLowerCase() === letter) {
                    allPs[letterIndex].textContent = value;
                    word[letterIndex] = "+";
                    k += 1
                    winCheck += 1
                }
            })
            if (k === 0) {
                attemptCounter -= 1;
                atts.textContent = `Attempts remaining ${attemptCounter}`;
                scoreBoostChangeable = 1
                boostStatus.textContent = `Streak: x${scoreBoostChangeable-1}`
            } else {
                scoreCounter += scoreBoost * scoreBoostChangeable * oneLetterScoreGiven;
                scoreBoostChangeable += 1;
                boostStatus.textContent = `Streak: x${scoreBoostChangeable-1}`
                if (maxBoost <= scoreBoostChangeable) {
                    maxBoost = scoreBoostChangeable;
                }
            }
        }
        const element = document.getElementById('word');
        const btn = document.getElementById('submit');
        btn.addEventListener('click', e => {
            e.preventDefault();
            if (word.length !== winCheck && attemptCounter !== 0) {
                mainScript(word, element.value.toLowerCase());
            }
            element.value = ''
            if (attemptCounter === 0) {
                let restart = document.getElementById("lost");
                restart.classList.remove("not-shown")
                const scrOutput = document.getElementById("scoreCountForLoss");
                scrOutput.textContent = `Score: ${scoreCounter}`;
                const maxBoostStatus = document.getElementById("maxBoostForLoss");
                if (maxBoost !== 0) {
                    maxBoostStatus.textContent = `Max Streak: ${maxBoost-1}`;
                } else {
                    maxBoostStatus.textContent = `Max Streak: ${maxBoost}`;
                }
            } else if (word.length === winCheck) {
                const restart = document.getElementById("won");
                if (attChecker === attemptCounter) {
                    const isPerfect = document.getElementById("perfect");
                    isPerfect.classList.remove("not-shown")
                }
                const attOutput = document.getElementById("AttemptCC");
                attOutput.textContent = `Wrong characters: ${attChecker - attemptCounter}`;
                const scrOutput = document.getElementById("scoreCountForWin");
                scrOutput.textContent = `Score: ${scoreCounter}`;
                const maxBoostStatus = document.getElementById("maxBoostForWin");
                if (maxBoost !== 0) {
                    maxBoostStatus.textContent = `Max Streak: ${maxBoost-1}`;
                } else {
                    maxBoostStatus.textContent = `Max Streak: ${maxBoost}`;
                }
                restart.classList.remove("not-shown")
            }
        });
    }
});



