// function customForEach(callbackFunction, array) {
//   for (let i = 0; i < array.length; i++) {
//     callbackFunction(array[i], i, array)
//   }
// }

// function customMap(callbackFunction, array) {
//   let newArray = [];
//   for (let i = 0; i < array.length; i++) {
//     newArray.push(callbackFunction(array[i], i, array))
//   }
//   return newArray;
// }

// function customFilter(callbackFunction, array) {
//   let newArray = [];
//   for (let i = 0; i < array.length; i++) {
//     if(callbackFunction(array[i], i, array)) {
//       newArray.push(array[i])
//     }
//   }
//   return newArray;
// }

/* ЗАДАЧА - 1
 * Написать функцию, которая не принимает аргументов и спрашивает пользователя что он есть на завтрак
 * Считается что пользователь может ввести только одно блюдо за один раз
 * По этому нужно продолжать спрашивать ДО ТЕХ ПОР ПОКА ответом от пользователя не прийдет пустой ввод.
 * КАЖДОЕ БЛЮДО, которое введет пользователь нужно помещать в массив fantasticBreakfast, который
 * и будет возвращаемым значением функции.
 * */

// function questions() {
// 	let fantasticBreakfast = [];
// 	let meal = prompt('Что вы будете на завтрак');
// 	while (meal !== '') {
// 		fantasticBreakfast.push(meal);
// 		meal = prompt('Что вы будете на завтрак еще?');
// 	}
// 	return fantasticBreakfast;
// }

// let result = questions();
// console.log(result);

/* ЗАДАЧА - 2
 * Написать функцию, которая будет принимать аргументом массив, в котором каждый елемент - название блюда которое пользователь ест на завтрак.
 * Нужно поочередно вывести в консоль каждое блюдо из этого массива. При этом удаляя данное блюдо из массива.
 * Таким образом после того как все блюда окажутся в консоли - массив должен остаться пустым
 * Возвращаемое значение - отсутствует
 * Сделать это при помощи классического цикла forEach
 * */

// function meal(allMeal) {
// 	while (allMeal.length !== 0) {
// 		console.log(allMeal[0]);
// 		allMeal.shift();
// 	}
// }

// meal(result);

/* ЗАДАЧА - 3
 * Написать функцию, которая принимает в качестве аргумента массив
 * Возвращаемое значение - новый, другой массив со всеми елементами того, который был передан агрументом
 * Сделать это при помощи: обычного циклка for, при помощи метода массива map()
 */

const listsStudent = [
	'Едик',
	'Даня',
	'Платон',
	'Саша',
	'Даша',
	'Андрей',
	'Настя',
	'Лера',
	'Лёша',
	'Саша В',
	'Коля',
	'Марьяна',
];

// function mirorArray(array) {
// 	const miror = [];
// 	for (let i = 0; i < array.length; i++) {
//         miror.push(array[i].toUpperCase())
//     }
//     return miror
// }
// function mirorArray(array) {
// 	const miror = array.map(item => {
// 		return item.toUpperCase();
// 	});
// 	return miror;
// }
// function mirorArray(array) {
// 	return array.map(item => item.toUpperCase());
// }
// const mirorArray = array => array.map(item => item.toUpperCase());

// let returnFunc = mirorArray(listsStudent);
// console.log(returnFunc);

/* ЗАДАЧА - 4
 * Написать функцию getItemList(), которая будет получать от пользователя строку с перечисленными через запятую названиями товаров
 * (они могут повторяться). После этого нужно преобразовать строку в МАССИВ С УНИКАЛЬНЫМИ ЗНАЧЕНИЯМИ
 */
//

/* ЗАДАЧА - 5
 * есть некий "склад", он же исходный массив, внутри которого лежат названия товаров.
 * Пользователь желает вместо определенного товара(!одного!) вставить один или несколько новых.
 *
 * НУЖНО написать функцию replaceItems(insteadOf, insertValue):
 * где insteadOf хранит строчное значение названия товара ВМЕСТО какого именно товара он будет вставлять новые.
 * ОБЯЗАТЕЛЬНО ПРОВЕРИТЬ ЕСТЬ ЛИ ТАКОЙ ТОВАР НА СКЛАДЕ, если товара нет - спросить у пользователя корректные данные.
 * insertValue - список елементов которые нужно вставить - один или несколько
 *
 * ВОЗВРАЩАЕМОЕ ЗНАЧЕНИЕ - отсутствует
 * В исходном массиве(складе) ЗАМЕНИТЬ указанный товар, НА insertValue так, чтобы длинна исходного массива изменилась, т.е. каждый введенный пользователем товар добавляется отдельным товаром в уже существующий склад.
 */
